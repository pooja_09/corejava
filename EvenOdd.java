// if else condition simple program

public class EvenOdd{
	public static void main (String args []){
	
	int num = Integer.parseInt(args[0]);

	if(num % 2 == 0){
		System.out.println(" Number "+num+" is even number ");
	
	}
	
	if(num % 2 != 0){
		System.out.println(" Number "+num+" is odd number ");
	
	}
	
	
	} 

}
