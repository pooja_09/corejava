
package twodarray;

import javax.swing.JOptionPane;

public class TwoDArray {

    public static void main(String[] args) {
        int x[][] = new int[2] [3];
        
        x[0][0] = 1;
        x[0][1] = 2;
        x[0][2] = 3;
        x[1][0] = 4;
        x[1][1] = 5;
        x[1][2] = 6;   
        
        System.out.println("2D array elem " +x[1][1] );
        
      
        for (int i = 0; i < 2; i++) {
            
            for (int j = 0; j < 3; j++) {
                
                String Array_IP= JOptionPane.showInputDialog("Enter x[" +  i + "][" +j +"]");
                
                x[i][j] = Integer.parseInt(Array_IP);
            }
            //System.out.println(" ");
        }
        
        int sum = 0;
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(x[i][j] + " ");
                sum=sum+x[i][j];
                
            }
            System.out.println("");
            
        }
        
        System.out.println("Sum Of Array Elem " + sum); 
        
        int total = 0;
        for (int i = 0; i < 2; i++) {
            total =0;
            for (int j = 0; j < 3; j++) {
                
                total=total+x[i][j];
                
            }
            System.out.println("row " +(i+1) + total);
            
        }
          
        
    }
    
}
