// to check no is even or odd by using if else statement

public class Ifelse1{
	public static void main (String args[]){
	
	int a = Integer.parseInt(args[0]);

	if (a % 2 ==0){
		System.out.println("Number "+a+" is even");
	
	}else {
		System.out.println("Number "+a+" is odd");
	}
	
	
	}

}
