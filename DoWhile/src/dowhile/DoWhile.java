package dowhile;

import javax.swing.JOptionPane;

public class DoWhile {

    public static void main(String[] args) {
        int x;
        do {
            
               String first_no = JOptionPane.showInputDialog(" Enter Number");
        
               x = Integer.parseInt(first_no);
        
     
            System.out.println( x + " Hello World!");
        } while (x !=0 );
    }
    
}
