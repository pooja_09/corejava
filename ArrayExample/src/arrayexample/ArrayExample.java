
package arrayexample;

import javax.swing.JOptionPane;


public class ArrayExample {

   
    public static void main(String[] args) {
        
        int sub[] = new int[5];
        sub[0] = 25;
        sub[1] = 30;
        sub[2] = 40;
        sub[3] = 45;
        sub[4] = 50;
       
        int subNew[] = {45,40,50};
        
        int totalMark = sub[0]+sub[1]+sub[2]+sub[3]+sub[4];
        System.out.println("Total Marks " + totalMark);
        
        int total = subNew[0]+subNew[1]+subNew[2];
        System.out.println("Total Marks " + total);
        
        float percent = ((float)totalMark/500)*100;
        System.out.println("Percent Marks " + percent );
        
        float percentNew = ((float)total/300)*100;
        System.out.println("Percent Marks " + percentNew );
        
        
        String InputMarks = JOptionPane.showInputDialog("Enter Marks");
        
        int x=Integer.parseInt(InputMarks);
        
    }
    
}
