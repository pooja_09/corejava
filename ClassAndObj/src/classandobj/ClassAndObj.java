
package classandobj;



        

class Circle {
    int radius;
    public void area(){
        double area = 3.14 * this.radius * this.radius;
        System.out.println("Area of circle is " + area);
    }
    
    public void setRadius(int r ){
        this.radius = r;
    }
}
public class ClassAndObj {

    
    public static void main(String[] args) {
        
        Rectangle r = new Rectangle(); //default constructor
        //r.setBreadth(5);
        //r.setLength(5);
        r.area();
        
        Rectangle s = new Rectangle(); //with setr
        s.setBreadth(5);
        s.setLength(5);
        s.area();
        
        Rectangle g = new Rectangle (3,5); //constructor with parameters
        g.area();
            
        Circle cobj = new Circle();
        cobj.setRadius(5);
        cobj.area();
      
    }
    
}
