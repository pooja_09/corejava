
package javaapplication17;

import javax.swing.JOptionPane;


public class UserInput {
    
    public static void main(String[] args) {
        String first_no = JOptionPane.showInputDialog(" Enter Number");
        
        int a = Integer.parseInt(first_no);
        System.out.println(" square of entered no " + a*a);
        
    }
    
    
}
