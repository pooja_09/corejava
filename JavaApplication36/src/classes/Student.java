/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.util.Objects;

/**
 *
 * @author pc
 */
public class Student {
    String name;

    @Override
    public int hashCode() {
        int hash = 5;
        hash = hash * this.name.hashCode();
        hash = hash * this.marks;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Student other = (Student) obj;
        if (this.marks != other.marks) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }
    int marks;

    public Student(String name, int marks) {
        this.name = name;
        this.marks = marks;
    }
}
