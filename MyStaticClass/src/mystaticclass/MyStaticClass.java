package mystaticclass;

class CompanyName{
    public static String cName= "ICIT";
    int cID;

    public CompanyName(int cID) {
        this.cID = cID;
    }
    
    public static void getcName(){

        System.out.println("Company Name " + CompanyName.cName);
        
    }
}

public class MyStaticClass {

    public static void main(String[] args) {
        
//        CompanyName c = new CompanyName();
//        c.getcName();

//CompanyName c = new CompanyName(2);

     CompanyName.getcName();
        
        
                
    }
    
}
