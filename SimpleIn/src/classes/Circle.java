/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import interfaces.Shape;

/**
 *
 * @author ICIT
 */
public class Circle extends Shape {
    int radius;

    public Circle() {
        radius = 1;
    }

    public Circle(int radius) {
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    @Override
    public String toString() {
        return "Circle{" + "radius=" + radius + '}';
    }
    
    @Override
    public void areaFun() {
        double area = this.radius * this.radius * 3.14;
        System.out.println("Area of circle is " + area);
    }
    
    public void circumFun(){
        double circum = this.radius * 2 * 3.14;
        System.out.println("Cirumference of circle is " + circum);
    }

   
}
