/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simplein;

import classes.Circle;
import interfaces.Shape;
import classes.Rectangle;
import classes.Student;
import classes.B;


/**
 *
 * @author ICIT
 */
public class SimpleIn {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Circle c = new Circle();
        c.setRadius(2);
        c.areaFun();
        c.circumFun();
        
        Shape s = new Circle(5);
        
        s.areaFun();
        
        
        Shape r = new Rectangle(10,7);
       // r.areaFun();
        
        Rectangle r1 = (Rectangle) r;
        r1.areaFun();
        
        B b = new B(5,9); 
       
        
       
//        Student s1 = new Student();
//        System.out.println(s1.getClass());
//        System.out.println(System.out.getClass());
//        System.out.println(s1 instanceof classes.Student);
//        
        Student s2 = new Student("pooja",2,80);
        System.out.println(s2.hashCode());
        
        Student s3 = new Student("pooja",2,80);
        System.out.println(s3.hashCode());
        
        System.out.println(s2.equals(s3));
        
        String s4 = "Hello";
        String s5 = "Hello";
        System.out.println(s4.hashCode());
        System.out.println(s5.hashCode());
        
        System.out.println(s4.equals(s5));
        
        
    }
    
}
