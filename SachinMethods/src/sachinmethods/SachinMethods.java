package sachinmethods;

class MyFunction {
    public void printHello(){
        System.out.println("Hello User How are you?");
    }
    
    public int add(int x, int y) {
        System.out.println("I am in add function");
        int z = x + y;
        return z;
    }
    
    public int sub (int x , int y) {
        System.out.println("I am in sub function");
        int z = x - y ;
        return z;
    }
    
    public float div (float x , float y) { 
        System.out.println("I am in division function");
        float z = x % y;
        return z;
      
    }
    
    public int mult ( int x , int y) {
        System.out.println("I am in mult function");
        int z = x* y;
        return z;
    }
    
    
}

public class SachinMethods {

    public static void main(String[] args) {
        System.out.println("Hello World");
        
        MyFunction mf = new MyFunction();
        mf.printHello();
        int addition_value = mf.add(5, 6);
        System.out.println("Addition is " + addition_value);
        
        int subtraction_value = mf.sub(8, 12);
        System.out.println("Subtarction is " + subtraction_value);
        
       float division_value = mf.div(89, 9);
        System.out.println("Division is " + division_value);
        
        int multi_value = mf.mult(8, 10);
        System.out.println("multiply is " + multi_value);
   
    
    
}
