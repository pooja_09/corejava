package arrayofobj;

/**
 * Inheritance example. this is a parent class.
 * @author pc
 */
public class Person {
    
    String name;
    int age;
    String city;
    int phn_no;

    public Person(String name, int age, String city, int phn_no) {
        this.name = name;
        this.age = age;
        this.city = city;
        this.phn_no = phn_no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getPhn_no() {
        return phn_no;
    }

    public void setPhn_no(int phn_no) {
        this.phn_no = phn_no;
    }

    @Override
    public String toString() {
        return "Person{" + "name=" + this.name + ", age=" + this.age + ", city=" + this.city + ", phn_no=" + this.phn_no + '}';
    }
    
    
    
    
    
    
}
