package arrayofobj;

/**
 *
 * @author pc
 */
public class Student extends Person {
    
    int roll_no;
    int marks;

    public Student(int roll_no, int marks, String name, int age, String city, int phn_no) {
        super(name, age, city, phn_no);
        this.roll_no = roll_no;
        this.marks = marks;
    }

    public int getRoll_no() {
        return roll_no;
    }

    public void setRoll_no(int roll_no) {
        this.roll_no = roll_no;
    }

    public int getMarks() {
        return marks;
    }

    public void setMarks(int marks) {
        this.marks = marks;
    }

    @Override
    public String toString() {
        return super.toString()+"Student{" + "roll_no=" + roll_no + ", marks=" + marks + '}';
    }
    
    
    
    
}
