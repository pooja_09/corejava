public class CircleDemo
{
   public static void main(String args[])
   {
      int radius = 5;
      double area = 3.14 * (radius * radius);
      float a = (float)area;
      System.out.println("circle area : " + area);

      double circumference= 3.14 * 2*radius;
      
      float x = (float)circumference;
      System.out.println( "circumference :"+x) ;
   }
}
