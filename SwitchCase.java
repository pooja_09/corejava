// to read single digit no and read it in string

public class SwitchCase{

	public static void main(String args[]){
	
	int a = Integer.parseInt(args[0]);

		switch (a){
		
			case 0:
			  System.out.println("Zero");
			  break;
			
			case 1:
			  System.out.println("One");
			  break;

			case 2:
			  System.out.println("Two");
			  break;

			case 3:
			  System.out.println("Three");
			  break;

			case 4: 
			  System.out.println("Four");
			  break; 
			
			default : 
			  System.out.println("Invalid Number");

		
		
		}
	
	}


}
